<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'summary' => 'first interview',
                'date_interview' => '2020-06-25',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'summary' => 'second interview',
                'date_interview' => '2020-07-01',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'summary' => 'manager interview',
                'date_interview' => '2020-07-08',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'summary' => 'las interview',
                'date_interview' => '2020-07-14',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                  
            ]); 
    }
}
