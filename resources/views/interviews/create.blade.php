@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date_interview">Interview date</label>
            <input type = "date" class="form-control" name = "date_interview">
        </div>     
        <div class="form-group">
            <label for = "summary">Summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div> 
        <div class="form-group">
            <label for="candidate_id">Candidate</label>
                <select class="form-control" name="candidate_id">                                                                         
                   @foreach ($candidates as $candidate)
                     <option value="{{ $candidate->id }}"> 
                         {{ $candidate->name }} 
                     </option>
                   @endforeach    
                 </select>
        </div>
        <div class="form-group">
            <label for="user_id">User</label>
                <select class="form-control" name="user_id">                                                                         
                   @foreach ($users as $user)
                     <option value="{{ $user->id }}">  {{ $user->name }} </option>
                   @endforeach    
                 </select>
            
        </div>
        <div>            
        <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
